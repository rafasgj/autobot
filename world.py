#!/usr/bin/env python

from math import sqrt

class World:
    '''
    Stores a bi-dimensional matrix representing the world map.
    The map representation is column x row (map[column][row]),
    and the size of the rows may vary.
    The width of the map is the width of the largest row.
    The height of the map is the number of columns.
    '''
    def __init__(self, filename):
        self.map = None
        self.load(filename)
        self.width = max([len(x) for x in self.map])
        self.height = len(self.map)
        self.ray = sqrt(self.width*self.width + self.height*self.height) / 2.0

    def load(self, filename):
        f = open(filename,"r")
        self.map = []
        for l in f.readlines():
            l = l.strip()
            data = [0]*len(l)
            self.map.append(data)
            for i in range(len(l)):
                if l[i] == '*':
                    data[i] = 1
                else:
                    data[i] = 0
        f.close()

    def get(self, x, y):
        '''
        Returns the value of a map cell in the set [0;1], where 0 is
        a free cell, 1 is a blocked cell. Any other value is a 'weight'
        of how hard it is to walk through the cell.
        '''
        if y >= self.height:
            return 1
        row = self.map[y]
        if x >= len(row):
            return 1
        return row[x]

