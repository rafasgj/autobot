#!/usr/bin/env python

from math import pi, sin, cos
from random import gauss
from util import euclidean_distance

class Sensor:
    '''
    Implements a simple rangefinder sensor.
    '''
    def __init__(self):
        self.noise = 0.001
        self.orientation = 0

    def set_noise(self, noise):
        self.noise = noise

    def sense(self, particle, world):
        x = round(particle.x)
        y = round(particle.y)
        o = self.orientation + particle.orientation
        m_x = round(x + cos(o) * world.ray)
        m_y = round(y + sin(o) * world.ray)
        x,y = self.bresenham_search(int(x),int(y),int(m_x),int(m_y),world)

        dist = euclidean_distance((x,y,),(particle.x,particle.y,))
        return dist + gauss(0,self.noise)

    def bresenham_search(self,x0,y0,x1,y1,world):
        '''
        using Bresenham's line algorithm find where there's an obstacle in the world
        '''
        dx = abs(x1 - x0)
        dy = abs(y1 - y0)
        sx = x0 < x1 and 1 or -1
        sy = y0 < y1 and 1 or -1
        err = dx - dy
        x,y = x0,y0
        while not (x == x1 and y == y1):
            if world.get(x, y) == 1:
                return (x,y)
            e2 = err * 2
            if e2 > -dy:
                err -= dy
                x += sx
            if e2 < dx:
                err += dx
                y += sy
        return (x,y)

    def set(self, orientation):
        self.orientation = orientation

    def rotate(self, alpha):
        '''Rotate the sensor by an angle (degrees)'''
        theta = alpha*pi/180
        # TODO add a turn noise for the sensor.
        self.orientation += theta
        self.orientation %= 2*pi

if __name__ == "__main__":
    from world import World

    class Particle:
        def __init__(self):
            self.x = 30
            self.y = 25
            self.orientation = 0
    
    w = World("data/map.txt")
    p = Particle()
    s = Sensor()
    s.set_noise(0.05)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
    print "Orientation:",s.orientation,"=",s.sense(p,w)
    s.rotate(45)
