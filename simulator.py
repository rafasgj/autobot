#!/usr/bin/env python

from particle import *

from Tkinter import *

import random,sys

from world import World

PARTICLE_COUNT = 1000

class RobotSimulator(Frame):
    def __init__(self, master, filename="data/map.txt"):
        size = 8
        self.master = master
        
        print "Creating world...",
        sys.stdout.flush()
        self.world = World(filename)
        print "done"
        
        self.width = self.world.width*8
        self.height = self.world.height*8 + 20

        print "creating image for a %d x %d map..." % (self.world.width,self.world.height),
        sys.stdout.flush()
        self.image = PhotoImage( width=self.width, height = self.world.height*8)
        print "done"
        
        print "creating particles...",
        sys.stdout.flush()
        self.particles = self.filter_particles([Particle(self.world) for _ in range(PARTICLE_COUNT)])
        # particles must not have 0 noise on sensors.
        for p in self.particles:
            p.set_noise(0.05, 0.05, 5.0)

        print "done."
        print "Using %d particles." % len(self.particles)

        print "creating robot...",
        sys.stdout.flush()
        self.robot = Particle(self.world)
        self.robot.set(30,20,0)
        print "done"

        w = Canvas(self.master,width=self.world.width*8,height=self.world.height*8)
        w.create_image((1,1),image=self.image,anchor=NW)
        w.pack(side=TOP)
        b2 = Button(self.master,text="LEFT",command=self.turn_left)
        b2.pack(side=LEFT,padx=10,fill=X,expand=0.3)
        b1 = Button(self.master,text="FRONT",command=self.front)
        b1.pack(side=LEFT,padx=10,fill=X,expand=0.3)
        b3 = Button(self.master,text="RIGHT",command=self.turn_right)
        b3.pack(side=LEFT,padx=10,fill=X,expand=0.3)

        self.update_map()

    def update_map(self):
        size = 8
        print "Drawing map...",
        sys.stdout.flush()
        self.draw_map(size)
        print "done"
        
        print "Drawing particles...",
        sys.stdout.flush()
        for p in self.particles:
            self.draw_particle(p)
        print "done."

        print "drawing Robot...",
        sys.stdout.flush()
        self.draw_robot(self.robot)
        print "done."

    def filter_particles(self, particles):
        return [ p for p in particles if self.world.get(int(p.x),int(p.y)) != 1]

    def load_map(self, filename):
        f = open(filename,"r")
        map = []
        for l in f.readlines():
            l = l.strip()
            data = [0]*len(l)
            map.append(data)
            for i in range(len(l)):
                if l[i] == '*':
                    data[i] = 1
                else:
                    data[i] = 0
        f.close()
        return map

    def draw_map(self,size=8):
        for y in range(self.world.height):
            for x in range(self.world.width):
                color = "#dddddd"
                if self.world.get(x,y) == 1:
                    color = "#000000"
                for i in range(size):
                    for j in range(size):
                        self.image.put(color, (x*size + i, y*size + j))

    def draw_robot(self, particle, color="#0000ff", size=8):
        x = (particle.x * size)
        y = (particle.y * size)
        
        for i in range(size):
            for j in range(size):
                self.image.put(color, (int(x+i),int(y + j)))

    def draw_particle(self, particle, color="#ff0000", size=2, factor=8):
        x = int((particle.x * factor) - factor/2.0)
        y = int((particle.y * factor) - factor/2.0)
        for i in range(size):
            for j in range(size):
                self.image.put(color, (x+i,y+j))

    def move_robot(self,turn,forward):
        self.robot = self.robot.move(turn,forward)
        Z = self.robot.sense()
        
        self.particles = [ p.move(turn,forward) for p in self.particles ]

        print "Resampling...",
        sys.stdout.flush()
        self.particles = self.filter_particles(resample(self.particles, Z, PARTICLE_COUNT))
        print "%d particles." % len(self.particles),
        print "done"
        self.update_map()

    def front(self):
        print "Moving forward..."
        self.move_robot(0,1)

    def turn_left(self):
        print "Turning Left..."
        self.move_robot(15/(2*pi),0)
    
    def turn_right(self):
        print "Turning Right..."
        self.move_robot(-15/(2*pi),0)

############

def fill(image, color):
    width = image.width()
    height = image.height()
    r,g,b = color
    hex = "#%02x%02x%02x" % (r,g,b)
    line = "{" + " ".join([hex]*width) + "}"
    image.put(" ".join([line]*height))

if __name__ == "__main__":
    root = Tk()
    root.title("Particle Filter Robot Simulator")
    filename = "data/map.txt"
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    simulator = RobotSimulator(root, filename)
    root.mainloop()
