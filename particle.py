#!/usr/bin/env python

from math import pi,sin,cos,sqrt,exp
from random import random, gauss
from sensor import Sensor

class Particle:
    def __init__(self, world):
        self.world = world
        self.x = random() * world.width
        self.y = random() * world.height
        self.orientation = random() * 2 * pi
        self.forward_noise = 0
        self.turn_noise = 0
        self.sensors = [Sensor()]

    def set(self, x, y, orientation):
        if x < 0 or x >= self.world.width:
            raise ValueError, "X value must be between 0 and",world.width
        if y < 0 or y >= self.world.height:
            raise ValueError, "Y value must be between 0 and",world.height
        self.x = float(x)
        self.y = float(y)
        self.orientation = float(orientation) % (2*pi)

    def set_noise(self, f, t, s):
        self.forward_noise = f
        self.turn_noise = t
        for sensor in self.sensors:
            sensor.set_noise(s)

    def gaussian(self, mu, sigma, x):
        if sigma == 0:
            return 0.000001
        return exp(-((mu-x)**2) / (sigma * sigma * 2.0)) / (sqrt(2.0*pi) * sigma)

    def sense(self):
        Z = []
        for sensor in self.sensors:
            for i in (0, 45, 90, 135, 180, 225, 270, 315):
                sensor.set(i)
                Z.append(sensor.sense(self,self.world))
        return Z

    def move(self, turn, forward):
        if forward < 0:
            raise ValueError, "Cannot move a negative amount."
        # turn
        self.orientation += turn + gauss(0, self.turn_noise)
        self.orientation %= 2*pi
        # move
        dist = forward + gauss(0, self.forward_noise)
        dx = cos(self.orientation) * dist
        dy = sin(self.orientation) * dist
        
        x = (self.x + dx) % self.world.width
        y = (self.y + dy) % self.world.height
        
        r = Particle(self.world)
        r.set(x,y,self.orientation)
        r.set_noise(self.forward_noise, self.turn_noise, self.sensors[0].noise)
        return r

    def measurement_prob(self, measurement):
        prob = 1.0;
        Z = self.sense()
        for i in range(len(measurement)):
            prob *= self.gaussian(Z[i], self.sensors[0].noise, measurement[i])
        return prob

    def __repr__(self):
        return "[ x=%g  y=%g orientation=%g ]" % (self.x,self.y,self.orientation)

def resample(particles, robot_sensors, N):
    r = []
    w = [ p.measurement_prob(robot_sensors) for p in particles ]
    index = int(random() * len(w))
    wmax = max(w)
    b = 0
    for i in range(N):
        b += random() * (2*wmax)
        while w[index] < b:
            b -= w[index]
            index = (index + 1) % len(w)
        r.append(particles[index])
        index = (index + 1) % len(w)
    return r

