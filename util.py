#!/usr/bin/env python

from math import sqrt

def euclidean_distance(p0,p1):
    a = p1[0] - p0[0]
    b = p1[1] - p0[1]
    return sqrt(a*a + b*b)

